//
//  SubscriptionCell.swift
//  Youtube
//
//  Created by Mark Davidson on 10/6/18.
//  Copyright © 2018 Mark Davidson. All rights reserved.
//

import UIKit

class SubscriptionCell: FeedCell {
    
    override func fetchVideos() {
        ApiService.sharedInstance.fetchSubscriptionFeed { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
        }
        
    }
}
