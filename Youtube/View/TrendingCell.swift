//
//  TrendingCell.swift
//  Youtube
//
//  Created by Mark Davidson on 10/6/18.
//  Copyright © 2018 Mark Davidson. All rights reserved.
//

import UIKit

class TrendingCell: FeedCell {
    
    override func fetchVideos() {
        ApiService.sharedInstance.fetchTrendingFeed { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
        }
        
    }
    
    
    
    
}
