//
//  Video.swift
//  Youtube
//
//  Created by Mark Davidson on 21/5/18.
//  Copyright © 2018 Mark Davidson. All rights reserved.
//

import UIKit

class Video: NSObject {
    
    var thumbnail_image_name : String?
    var title: String?
    var number_of_views: NSNumber?
    var uploadDate: NSDate?
    var durection: NSNumber?
    
    var channel: Channel? 
    
}

class Channel: NSObject {
    
    var name: String?
    
    var profile_image_name: String?
    
}
